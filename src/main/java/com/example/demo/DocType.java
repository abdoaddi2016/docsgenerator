package com.example.demo;

public enum DocType {
    AttestationPension("AttestationPension.docx", "AttestationPension.pdf"),
    AttestationPensionVirment("AttestationPensionVirment.docx","AttestationPensionVirment.pdf"),
    AttestationAffilie("AttestationAffilie.docx","AttestationAffilie.pdf");

    public final String template;
    public final String pdfName;

    DocType(String template, String pdfName) {
        this.template = template;
        this.pdfName = pdfName;
    }
}
