package com.example.demo.controller;

import com.example.demo.DocType;
import com.example.demo.service.DocsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DocsController {


    @Autowired
    private DocsService documentService;

    @GetMapping(value = "/downloadDoc")
    public ResponseEntity downloadDoc(@RequestParam("type") DocType type){
        try {

            Resource resource = documentService.generateDocument(type);
            String    contentType = "application/octet-stream";

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + type.template.replace("docx","pdf") + "\"")
                    .body(resource);
        }catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
