package com.example.demo.service;



import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.Docx4J;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;

@Service
public class DocPdfConvertor {



    byte[] toPdf2(XWPFDocument doc)throws Exception {
        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            doc.write(stream);
            stream.flush();
            final ByteArrayOutputStream out = new ByteArrayOutputStream();

            Docx4J.toPDF(Docx4J.load(new ByteArrayInputStream(stream.toByteArray())),out);
            return out.toByteArray();
        } catch (Exception e) {
                    throw new Exception("Erreur génération Pdf depuis docx ");
        }
    }
    byte[] toPdf(XWPFDocument doc){
        try{
            ByteArrayOutputStream outFile = new ByteArrayOutputStream( );

            PdfOptions options =  null;//PdfOptions.create().setConfiguration();
            PdfConverter.getInstance().convert(doc,outFile,options);

            outFile.close();
            doc.close();
            return outFile.toByteArray();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    void toPdf(String filePath, XWPFDocument doc){
        toPdf(filePath, doc, null);
    }

    void toPdf(String filePath, XWPFDocument doc, PdfOptions options){
        try{
            OutputStream outFile = new FileOutputStream( filePath);

            PdfConverter.getInstance().convert(doc,outFile,options);

            outFile.close();
            doc.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
