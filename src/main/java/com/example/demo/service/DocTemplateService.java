package com.example.demo.service;

import com.example.demo.DocType;
import com.example.demo.Utils;
import cyclops.control.Try;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.data.util.Pair;
import com.google.common.base.Strings;

import javax.annotation.PostConstruct;
import javax.print.Doc;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.demo.service.ValueTemplateMapper.*;

@Service
public class DocTemplateService {

    public static final String TAG_REGX = "\\<\\$\\w*\\$\\>";

    @Autowired
    ResourceLoader resourceLoader;

    @Autowired
    ValueTemplateMapper valueTemplateMapper;

    Map<DocType, Map<Integer,String>> docMapToken;

    @PostConstruct
    public void init(){
        Map<Integer, String> attestationPension = Utils.mapOf(1, NUMERO_SERIE, 2, GRADE, 3, ECHELLE, 4, ECHELON, 5, INDICE,6, ANNEES
        ,7,TAUX,8,FRACTION,9,NBR,10,ETAT_CIVIL,11,DATE_EFFET_PENSION,12,MONTANT_BRUT,13,MONTANT_ALLOCATION,14,TOTAL_BRUT,15,ASSURANCE_MALADIE_MT
        ,16,ASSURANCE_MALADIE_MUT,17,SECTEUR_MT,18,SECTEUR,19,CAISSE_COMPLI_MT,20,CAISSE_COMPLI,21,ASSURANCE_MT,22,IMPOT_REVENU,23,TOTAL_RETENU
        ,24,TOTAL_NET_ANNUEL,25,NET_MENSUEL,26,RAPPEL,27,MODE_PAIEMENT,28,BANQUE,29,DATE_EDIT,30,NAME,31,CIN,32,TYPE_PENSION,33,NUM_PENSION);
        Map<Integer, String> attestationPensionVirement =Utils.mapOf(1,NUM_PENSION,2,NAME,3,MONTANT_MENSUEL,4,RIB,5,BANQUE,6,DATE_EDIT);
        Map<Integer, String> AttestationAffilie = Utils.mapOf(1,NAME,2,CIN,3,DATE_NAISSAINCE);
        docMapToken = new HashMap<>();
        docMapToken.put(DocType.AttestationPension, attestationPension);
        docMapToken.put(DocType.AttestationPensionVirment,attestationPensionVirement);



    }
    public XWPFDocument generateDocFromTemplate(DocType docType)throws Exception{

        try {
            Resource resource = null;
            if(resource==null)
                resource = resourceLoader.getResource("classpath:"+docType.template);

            final Function<MatchResult,String> getTagValueFn = (tag)->getTagValue(docType, tag);

            XWPFDocument doc = new XWPFDocument(resource.getInputStream());
            doc = process(doc, getTagValueFn);
            return doc;

        }catch (Exception e){
            throw new Exception("erreur generation de document");
        }
    }

    private String getTagValue(DocType docType, MatchResult match) {
        String tag = match.group();
        //enlever les prefix et suffix du tag: <$   $>
        //String key = tag.substring(2, tag.length()-2);
        String key="";
        if(tag.startsWith("<$t0")) {
            Integer indexTag = Integer.parseInt(tag.replaceFirst("\\<\\$t", "").replace("$>", ""));
            key = docMapToken.get(docType).computeIfAbsent(indexTag,i->"Unknown:"+i);
        }else
            key = tag.substring(2, tag.length()-2);
        if(key == null)
            key = "";
        return valueTemplateMapper.getValueByKey(key);
    }

    public XWPFDocument process(XWPFDocument doc, Function<MatchResult, String> getTagValueFn) {

        doc.getParagraphs().forEach(p ->{
            replaceParagraphs(p, getTagValueFn);
        });
        doc.getTables().forEach(t->{
            t.getRows().forEach(r->{
                r.getTableCells().forEach(c->{
                    c.getParagraphs().forEach(p->{
                        replaceParagraphs(p, getTagValueFn);
                    });
                });
            });
        });

        return doc;
    }
    public  void replaceParagraphs(XWPFParagraph p, Function<MatchResult,String> getTagValueFn) {
        p.getRuns().forEach(run -> {
            try {
                int i=0;
                while(true) {
                    String text = run.getText(i);
                    if(text==null )
                        break;
                    List<MatchResult> tags = getTags(text);
                    for (MatchResult tag : tags) {
                        String tagValue = Strings.nullToEmpty(getTagValueFn.apply(tag));
                        run.setText(text.replace(tag.group(), tagValue), i);
                        text = run.getText(i);
                    }
                    i++;
                }
            }catch (IndexOutOfBoundsException e){  }


        });
        List<MatchResult> tags = getTags(p.getText());
        BiConsumer<Integer, String> setRunText = (i, t)->p.getRuns().get(i).setText(t,0);
        if(!tags.isEmpty()){
            final List<XWPFRun> runs = p.getRuns();
            IntFunction<String> getTexRun = (i)->runs.get(i).text();
            mergeRuns(setRunText,  getTexRun, runs.size());
            replaceParagraphs(p, getTagValueFn);
        }

    }
    private Pair<Integer, Integer> searchDecomposedTag(IntFunction<String> textRun, int runsSize) {
        String prefixTag = "<$";
        int indexStartRun=0;
        while( indexStartRun<runsSize) {
            int runStartTag = getRunStartOfTag(indexStartRun, textRun, runsSize, prefixTag);
            int runEndTag = -1;
            if (runStartTag != -1) {
                runEndTag = searchEndOfTag(textRun, runsSize, runStartTag);
            }
            if (runEndTag != -1)
                return Pair.of(runStartTag, runEndTag);
            indexStartRun++;
        }
        return null;
    }

    private int searchEndOfTag(IntFunction<String> textRun, int runsSize, int runStartTag) {
        for (int i = runStartTag; i <= runsSize ; ++i) {
            final String concat = concatRuns(textRun, runsSize, runStartTag, i);
            if(isTag(concat))
                return i;
        }
        return -1;
    }

    private boolean isTag(String input) {
        Pattern pattern = Pattern.compile(TAG_REGX);

        Matcher matcher = pattern.matcher(input);
        return  matcher.find();

    }
    private String concatRuns(IntFunction<String> textRun, int runsSize, int start, int end){
        String concat = "";
        for (int i = start; i < runsSize && i < end; i++) {
            concat += textRun.apply(i);
        }
        return concat;
    }

    public void mergeRuns(BiConsumer<Integer, String> setRunText,  IntFunction<String> getTexRun, int runsSize) {
        Pair<Integer,Integer> startEndTag = searchDecomposedTag(getTexRun, runsSize);
        if(startEndTag!=null){
            setRunText.accept(startEndTag.getFirst(),concatRuns(getTexRun, runsSize, startEndTag.getFirst(), startEndTag.getSecond()));
            for (int i = startEndTag.getFirst()+1; i < startEndTag.getSecond() && i < runsSize ; i++) {
                setRunText.accept(i,"");

            }
        }
    }
    private int getRunStartOfTag(int indexStartRun,IntFunction<String> textRun, int runsSize, String prefixTag){
        for (int i = indexStartRun; i < runsSize; i++) {
            if(textRun.apply(i).contains(prefixTag.substring(0,1)))
                return i;
        }
        return -1;
    }
    public List<MatchResult> getTags(String input){
        Pattern pattern = Pattern.compile(TAG_REGX);


        Matcher matcher = pattern.matcher(input);
        List<MatchResult> tags = new ArrayList<>();
        while(matcher.find()){
            tags.add(matcher.toMatchResult());
        }
        return tags;
    }
}
