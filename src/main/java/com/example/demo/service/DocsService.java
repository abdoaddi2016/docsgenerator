package com.example.demo.service;

import com.example.demo.DocType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

@Service
public class DocsService {

    @Autowired
    DocTemplateService docTemplateService;

    @Autowired
    DocPdfConvertor docPdfConvertor;

    public ByteArrayResource generateDocument(DocType type)throws Exception{
        try {
            XWPFDocument doc = docTemplateService.generateDocFromTemplate(type);
            byte[] outputStream = docPdfConvertor.toPdf2(doc);
            ByteArrayResource byteArrayResource = new ByteArrayResource(outputStream);
            return byteArrayResource;

        }catch (Exception e){
            throw new Exception(e.getMessage());
        }

    }
}
