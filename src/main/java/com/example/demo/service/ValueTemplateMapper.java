package com.example.demo.service;

import cyclops.control.Try;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.example.demo.Utils.getOrEmpty;

@Service
public class ValueTemplateMapper {



    public static final String NAME = "NAME";
    public static final String CIN = "CIN";
    public static final String TYPE_PENSION = "TYPE_PENSION";
    public static final String NUM_PENSION = "NUM_PENSION";
    public static final String NUMERO_SERIE = "NUMERO_SERIE";
    public static final String GRADE = "GRADE";
    public static final String ECHELLE = "ECHELLE";
    public static final String ECHELON = "ECHELON";
    public static final String INDICE = "INDICE";
    public static final String ANNEES = "ANNEES";
    public static final String TAUX = "TAUX";
    public static final String FRACTION = "FRACTION";
    public static final String NBR = "NBR";
    public static final String ETAT_CIVIL = "ETAT_CIVIL";
    public static final String DATE_EFFET_PENSION = "DATE_EFFET_PENSION";
    public static final String MONTANT_BRUT = "MONTANT_BRUT";
    public static final String MONTANT_ALLOCATION = "MONTANT_ALLOCATION";
    public static final String TOTAL_BRUT = "TOTAL_BRUT";
    public static final String ASSURANCE_MALADIE_MT = "ASSURANCE_MALADIE_MT";
    public static final String ASSURANCE_MALADIE_MUT = "ASSURANCE_MALADIE_MUT";
    public static final String SECTEUR_MT = "SECTEUR_MT";
    public static final String SECTEUR = "SECTEUR";
    public static final String CAISSE_COMPLI_MT = "CAISSE_COMPLI_MT";
    public static final String CAISSE_COMPLI = "CAISSE_COMPLI";
    public static final String ASSURANCE_MT = "ASSURANCE_MT";
    public static final String IMPOT_REVENU = "IMPOT_REVENU";
    public static final String TOTAL_RETENU = "TOTAL_RETENU";
    public static final String TOTAL_NET_ANNUEL = "TOTAL_NET_ANNUEL";
    public static final String NET_MENSUEL = "NET_MENSUEL";
    public static final String RAPPEL = "RAPPEL";
    public static final String MODE_PAIEMENT = "MODE_PAIEMENT";
    public static final String BANQUE = "BANQUE";
    public static final String DATE_EDIT = "DATE_EDIT";
    public static final String MONTANT_MENSUEL = "MONTANT_MENSUEL";
    public static final String RIB = "RIB";
    public static final String DATE_NAISSAINCE = "DATE_NAISSAINCE";


    public String getValueByKey(String key) throws TemplateException {
        switch (key){
            case NUMERO_SERIE: return NUMERO_SERIE;
            case GRADE: return GRADE;
            case ECHELLE: return ECHELLE;
            case ECHELON: return ECHELON;
            case INDICE: return INDICE;
            case ANNEES: return ANNEES;
            case TAUX: return TAUX;
            case FRACTION: return FRACTION;
            case NBR: return NBR;
            case ETAT_CIVIL: return ETAT_CIVIL;
            case DATE_EFFET_PENSION: return DATE_EFFET_PENSION;
            case MONTANT_BRUT: return MONTANT_BRUT;
            case MONTANT_ALLOCATION: return MONTANT_ALLOCATION;
            case TOTAL_BRUT: return TOTAL_BRUT;
            case ASSURANCE_MALADIE_MT: return ASSURANCE_MALADIE_MT;
            case ASSURANCE_MALADIE_MUT: return ASSURANCE_MALADIE_MUT;
            case SECTEUR_MT: return SECTEUR_MT;
            case SECTEUR: return SECTEUR;
            case CAISSE_COMPLI_MT: return CAISSE_COMPLI_MT;
            case CAISSE_COMPLI: return CAISSE_COMPLI;
            case ASSURANCE_MT: return ASSURANCE_MT;
            case IMPOT_REVENU: return  IMPOT_REVENU;
            case TOTAL_RETENU: return  TOTAL_RETENU;
            case TOTAL_NET_ANNUEL: return TOTAL_NET_ANNUEL;
            case NET_MENSUEL: return NET_MENSUEL;
            case RAPPEL: return RAPPEL;
            case MODE_PAIEMENT: return MODE_PAIEMENT;
            case BANQUE: return BANQUE;
            case DATE_EDIT: return DATE_EDIT;
            case NAME: return NAME;
            case CIN: return CIN;
            case TYPE_PENSION: return TYPE_PENSION;
            case NUM_PENSION: return NUM_PENSION;
        }
        return key;
        //throw new TemplateException("clé template inconnu clé:"+key);
    }



    public List<Integer> getIndexCase(String key, String caseName) {
        try{
            String[] sIndexs  = key.replace(caseName,"").split("o");
            return Arrays.stream(sIndexs).map(Integer::parseInt).collect(Collectors.toList());
        }catch (Exception e){
            throw new TemplateException("erreur extraction de l'index case du token clé:"+key,e);
        }
    }




    private class TemplateException extends RuntimeException {
        public TemplateException(String s) {
            super(s);
        }

        public TemplateException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    private String formatDate(Date date, String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }
}
