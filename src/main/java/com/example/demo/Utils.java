package com.example.demo;

import cyclops.control.Try;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Utils {

    public static <K, V> Map<K, V> mapOf(Object... keyValues) {
        Map<K, V> map = new HashMap<>();

        for (int index = 0; index < keyValues.length / 2; index++) {
            map.put((K)keyValues[index * 2], (V)keyValues[index * 2 + 1]);
        }

        return map;
    }

    public static Integer notEmptyIndedOr(Supplier<Integer> fn, Integer or){
        Integer value  = fn.get();
        if(value!=null && value!=-1)
            return value;
        return or;
    }
    public static void setIfIndexValid(Supplier<Integer> supplier, Consumer<Integer> consumer){
        Integer value  = supplier.get();
        if(value!=null && value!=-1)
            consumer.accept(value);
    }
    public static void setIfNotNull(Supplier<String> supplier, Consumer<String> consumer){
        String value  = supplier.get();
        if(value!=null)
            consumer.accept(value);
    }
    public static String getOrEmpty(Try.CheckedSupplier<String, NullPointerException> call){
        return Try.withCatch(call, NullPointerException.class).notNull().orElse("");
    }
    public static <T> T getOr(Try.CheckedSupplier<T, NullPointerException> call, T or){
        return Try.withCatch(call, NullPointerException.class).notNull().orElse(or);
    }

    public static long hoursToMillisec(int hours) {
        return minutsToMillisec(hours*60l);
    }

    public static long minutsToMillisec(long minutes) {
        return minutes*60000l;
    }

    public static String logFormat(String format, Object... args){
        return Try.withCatch(()->String.format(format, args)).orElse("erreur log format:"+format);
    }
}
